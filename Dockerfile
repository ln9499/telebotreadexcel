FROM python:3.12.1-slim-bullseye
WORKDIR /app
RUN chmod 777 /app
RUN python3 -m pip install -U pip
COPY . .
RUN pip3 install --no-cache-dir -U -r requirements.txt
CMD ["python", "main.py"]