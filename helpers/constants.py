from enum import Enum


class MessageHandlerCommands(Enum):
    START = "start"
    HELLO = "hello"
    READ_RECORD = "read_record"
    WRITE_RECORD = "write_record"
    SEND_LOCATION = "send_location"


class MessageSendContent(Enum):
    SUCCEED = "success"
