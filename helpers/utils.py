import requests
import pandas as pd
import numpy as np
from openpyxl import load_workbook

# def get_daily_horoscope(sign: str, day: str) -> dict:
#     """Get daily horoscope for a zodiac sign.

#     Keyword arguments:
#     sign:str - Zodiac sign
#     day:str - Date in format (YYYY-MM-DD) OR TODAY OR TOMORROW OR YESTERDAY
#     Return:dict - JSON data
#     """
#     url = "https://horoscope-app-api.vercel.app/api/v1/get-horoscope/daily"
#     params = {"sign": sign, "day": day}
#     response = requests.get(url, params)

#     return response.json()

# read by default 1st sheet of an excel file
download_link_prefix = "https://drive.google.com/uc?export=download&id="
def read_data_from_file(file_path):
    sheet_name = 'FoodSales'
    query_cols = ['ID','Date','City']
    download_link = get_google_sheets_download_link(file_path)
    if (download_link == None): return None
    else: 
        df = pd.read_excel(download_link, sheet_name=sheet_name, usecols=query_cols)
        df = df[(df['City']=='Los Angeles')]
        mat = np.array(df)
        return np.array2string(mat)

def get_google_sheets_download_link(file_path):
    file_split= file_path.split('/')
    if len(file_split) > 5:
        return download_link_prefix + file_split[5]
    else:
        return None

def write_data_to_file(file_path):
    sheet_name = 'Write_data'
    # workbook = load_workbook(file_path)
    df = pd.DataFrame([[11, 21, 31], [12, 22, 32], [31, 32, 33]],
                  index=['1', '2', '3'], columns=['A', 'B', 'C'])
    with pd.ExcelWriter(file_path) as writer:
        df.to_excel(writer, sheet_name=sheet_name)